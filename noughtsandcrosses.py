#!/usr/bin/python
"""
    Noughts n' Crosses game as replacement for CodeMonkeys... rubbish?
    Copyright (C) 2020 ACBob (ScreenName)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

#It's a terminal application.

import sys

if not sys.stdin.isatty():
    #Attempt to show an error.
    #AFAIK Most python installations have tkinter. Should work in most circumstances.
    #When it doesn't, they won't know, eh? ;)
    import tkinter
    from tkinter import messagebox
    root = tkinter.Tk()
    root.withdraw()
    messagebox.showerror("Error!","This is a terminal application.\nPlease run it in an interactive shell.")
    
    exit(-1)

game_board = [[0,0,0],
              [0,0,0],
              [0,0,0]]
board_size_x = 3
board_size_y = 3
playing = True
player = False # False and True become 0,1 respectively.
round_num = 0
graphics = {
    "board":{
        0: "#",
        1: "x",
        2: "o"
    },
    "player":{
        False: "Crosses",
        True: "Noughts"
    }
}

#ANSI Codes
CLR_DBG = u"\u001b[33m"
CLR_ERR = u"\u001b[31m"
CLR_CLS = u"\u001b[0m"

DEBUG = False

def debug_print(text):
    if DEBUG:
        print(CLR_DBG+text+CLR_CLS)
def error_print(text):
    print(CLR_ERR+text+CLR_CLS)

def get_board():
    """Returns the visual board"""
    visualboard = ""
    for line in game_board:
        for piece in line:
            visualboard += graphics["board"][piece]
        visualboard += "\n"
    return visualboard

def check_win(section,player):
    for i in section:
        if not i == player:
            return False

    return True
            

def test_board(player):
    """Tests the board for any 3 connections"""
    #I admit, i looked at different implementations of TicTacToe for this.
    #It was only this one part, everything else was chosen by me.
    #And this wasn't copy+paste, it was a simple look at implementation.
    #Despite taking the function name 'check_win' from an implementation i saw, i ended up getting an idea and running with it. Much like scissors.
    debug_print("Testing Horizontal")
    for y in range(board_size_y):
        if check_win(game_board[y],player):
            return [True, player]

    debug_print("Testing Vertical")
    for x in range(board_size_x):
        chunk = [game_board[0][x],game_board[1][x],game_board[2][x]]
        if check_win(chunk,player):
            return [True, player]

    debug_print("Testing \\")
    chunk = [game_board[0][0],game_board[1][1],game_board[2][2]]
    if check_win(chunk,player):
        return [True, player]

    debug_print("Testing /")
    chunk = [game_board[2][0],game_board[1][1],game_board[0][2]]
    if check_win(chunk,player):
        return [True, player]

    return [False, 0]

while playing:
    print("Round {}".format(round_num))
    print("{}' Turn".format(graphics["player"][player]))
    print(get_board())

    x = 0
    while x <= 0 or x > board_size_x:
        x = int(input("x: "))
    y = 0
    while y <= 0 or y > board_size_y:
        y = int(input("y: "))

    x = x-1
    y = y-1

    if game_board[y][x] == 0:
        game_board[y][x] = int(player)+1
    else:
        error_print("Illegal Move!")
        continue

    winner = test_board(int(player)+1)

    if winner[0]:
        print("{} Win!".format(graphics["player"][player]))
        playing = False
        break

    player = not player
    round_num += 1
