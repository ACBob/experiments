#Test TrivialBrainfuckSubstitution
# I could probably make it better in a few places, but i'm happy with this.
# While i did this due to boredom, it's taught me a few things.
# Interpereters aren't scary things - They're just programs like this.
# Brainfuck has simple operators, that are easy to cater for.
# But interpereters are just massive conditionals.
# I could probably extend the functionalities of this interpereter,
# To make my own flavour of brainfuck with some things. But it's
# finished for now.
#
# Oh yeah, i should probably mention, It mucks up if you run it in IDLE.
# IDLE replaces sys.stdout, and it doesn't contain a buffer.
# This program writes directly to the buffer, but since it's not there,
# It errors out because it's not defined.

import sys # Used in the input

#Stuff
global tape
tape = [0] * 30000 #30,000 as per usual
global tape_index
tape_index = 0

#Symbols
move_tape_forward = ">"
move_tape_backward = "<"
increment_current_value = "+"
decrement_current_value = "-"
print_current_value = "."
get_user_value = ","
skip_if_0 = "["
skip_back_ifnot_0 = "]"

test_program = """
[
This is a hello world program.
]

>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]
>++++++++[<++++>-] <.>+++++++++++[<++++++++>-]<-.--------.+++
.------.--------.[-]>++++++++[<++++>- ]<+.[-]++++++++++."""

def Interpret(program):
    global tape
    global tape_index

    instruction = 0
    while instruction < len(program):
        i = program[instruction] # Less to type

        if i == move_tape_forward:
            #print("Moving pointer +1")
            tape_index+=1
        elif i == move_tape_backward:
            #print("Moving pointer -1")
            tape_index-=1
        elif i == increment_current_value:
            #print("incrementing pointer value")
            tape[tape_index] += 1
        elif i == decrement_current_value:
            #print("Dec_current_valuerementing pointer value")
            tape[tape_index] -= 1
        elif i == print_current_value:
            #print("Printing Value")
            sys.stdout.buffer.write(bytes([tape[tape_index]]))
        elif i == get_user_value:
            #print("Recieving User Value")
            tape[tape_index] = ord(sys.stdin.read(1))
        elif i == skip_if_0:

            if tape[tape_index] == 0:
                #print("Skipping to next brace")
                instruction = program.find(skip_back_ifnot_0,instruction)
        elif i == skip_back_ifnot_0:

            if tape[tape_index] != 0:
                #print("Skipping to prev. brace")
                instruction = (len(program) - program[::-1].find(skip_if_0,-(instruction+1))) - 1

        instruction+=1



if __name__ == "__main__":
    try:
        Interpret(test_program)
    except IndexError:
        print("Reached end of tape!") # If you reach this with the usual 30,000 cells, I will personally write you a letter of "STOP WHAT YOU ARE DOING!"

    sys.stdout.flush()
    #print(tape)
