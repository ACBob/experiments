# Known Issues / Limitations
# 1. What are the units?
#	There aren't any units set, it's all magic numbers bb
# 2. Where's the documentation?
#	It's where-ever the "moon" goes after 16 years of looping around the planet
# 3. Shaking the screen will alter the physics, somehow
#	Like what? Maybe it's floating point inaccuracies? I'm too burnt out on this project to care, though.

import pygame, pygame.gfxdraw
import numpy as np

vec = pygame.math.Vector2
gravconst = 0.00674#0000000000674 #wow

class planet(object):

    def __init__(self,pos=(0,0),vel=(0,0),radius=1,surface_gravity=1,colour=(255,255,255)):
        self.pos = vec(pos)
        self.vel = vec(vel)
        self.acc = vec(0,0)

        self.colour = colour
        self.radius = radius
        self.surface_gravity = surface_gravity

        self.points = []

        self.mass = surface_gravity * radius * radius / gravconst

    def update_velocity(self,p,delta):
        # Based in part off of Sebastian Lague's implementation in unity
        for b in p:
            if b == self:
                continue
            dist = pow((b.pos - self.pos).magnitude(),2)
            frcd = (b.pos - self.pos).normalize()
            forc = frcd * gravconst * self.mass * b.mass / dist
            self.acc = forc / self.mass

            self.vel += self.acc *delta

    def update_position(self,delta):
        self.pos += self.vel *delta

    def add_path_point(self):
        #Add a path point for showing where we're roughly going
        self.points.append(np.round(self.pos))

        if len(self.points) > 100:
            self.points = self.points[-100:]

    def get_offset_points(self,display_size,por,zoom):
        t = []
        for i in self.points:
            t.append(((i + vec(display_size)/2) - por)*zoom)

        return t

    def render(self,display,por=(0,0),zoom=1): #por = Point of Reality (Like Camera)

        sight_size = display.get_size()

        if len(self.points) > 2:
            pygame.draw.lines(display, self.colour, False, self.get_offset_points(sight_size,por,zoom))


        renderpos = ((self.pos + vec(sight_size)/2) - por)
        pygame.draw.circle(display, self.colour, np.array(renderpos*zoom).astype(int), round(self.radius*zoom))


if __name__ == "__main__":
    a = planet((0,0),(0,0),radius=16,surface_gravity=10,colour=(255,0,0))
    b = planet((256,0),(0,3),radius=4,surface_gravity=2.5,colour=(255,255,0))
    c = planet((288,0),(0,1.76),radius=1,surface_gravity=1,colour=(255,255,255))

    planets = [a,b,c] #Sadly after a few of the solar system's "years" it breaks up and dies

    pygame.init()
    screen = pygame.display.set_mode((800,600))

    POINTEVENT = pygame.USEREVENT+1
    pygame.time.set_timer(POINTEVENT, 50)

    c = pygame.time.Clock()

    zoom = 1

    run = True
    while run:

        delta = c.tick(60) / 8

        screen.fill((0,0,0))

        pygame.display.set_caption(str(round(c.get_fps())))

        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                run = False
            elif e.type == POINTEVENT:
                for p in planets:
                    p.add_path_point()
            elif e.type == pygame.MOUSEBUTTONDOWN:
                if e.button == 1:
                    zoom -= 0.1
                elif e.button == 3:
                    zoom += 0.1

        for p in planets:
            p.update_velocity(planets, delta)
        for p in planets:
            p.update_position(delta)

        for p in planets:
            p.render(screen,a.pos, zoom)

        pygame.display.flip()

    pygame.quit()
