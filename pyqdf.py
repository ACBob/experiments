f='''
// Keys can have multiple values in parenthesis
one(two "three")
// or spread out over many statements
"two" three
three four
five
six
'''

GROUPTOKEN = "{"
GROUPCLOSETOKEN = "}"
LISTTOKEN = "("
LISTCLOSETOKEN = ")"
WHITESPACE = [" ","\t","\n"]

RESERVED = [GROUPTOKEN,LISTTOKEN] # Group of reserved to additionally break up tokens

def parse(f):
    # Run through f
    
    token = ""
    key = ""
    value = ""
    dick = {}

    line = 1
    character = 0

    i=0
    while i < len(f):
##        print(i,"/",len(f))
        i += 1
        c = f[i-1]

        # Line counting and shit
        # FIXME!! Not actually accurate as it doesn't take into account the magical jumping shit we do when parsing
        if c == "\n":
            line += 1
            character = 0
        else:
            character += 1

##        print("C",c,"T",token)
        
        if c == '"':
            # We're creating a spaced value
##            print("CREATE SPACED VALUE")

            # Find the bounds of the value, unlike groups we don't need to search backwards
            j = f.find('"',i)
            if j == -1: # If we can't, shout
                raise SyntaxError("Unclosed Spaced Value @ {line}:{character}".format(line=line,character=character))

            g = f[i:j]
            i = j + 1 # Skip ahead

            if not key:
                key = g
            elif not value:
                value = g

            if key and value:
                dick[key] = value
                key = ""
                value = ""

            continue
        if c == "/":
            if f[i] == "/":
                # We're a comment, search for the next line
                j = f.find('\n',i)
                if j == -1:
                    # We're the last thing in the file (hopefully) so skip
                    break
                i = j+1
                continue

        if c in RESERVED:
            # We have a character that isn't seperating a value per-say
            if token:
                # We have a token, but its not been set to our key. set it here.
                # This is because we can assume the current token is the key for this.
                # Removing the need for whitespace entirely.
                key = token
                token = ""

            if c == GROUPTOKEN:
                # We're creating a group
##                print("CREATE GROUP")

                # Find the bounds of the group, by searching backwards for a closing tag
                j = f[:i:-1].find(GROUPCLOSETOKEN)
                if j == -1: # If we can't, shout at the stupid, stupid user
                    raise SyntaxError("Unclosed Group @ Line {line}:{character}".format(line=line,character=character))
                j = len(f) - j + 1

                if not key:
                    raise SyntaxError("Group opened with no key @ Line {line}:{character}".format(line=line,character=character))
                dick[key] = parse(f[i:j])
                key = ""
                # Skip the group for the rest of parsing
                i = j+1

            if c == LISTTOKEN:
                # We're creating a list
##                print("CREATE LIST")

                # Find the bounds of the list, by searching backwards for a closing tag
                j = f[:i:-1].find(LISTCLOSETOKEN)
                if j == -1: # If we can't, shout at the stupid, stupid user
                    raise SyntaxError("Unclosed List @ Line {line}:{character}".format(line=line,character=character))
                j = len(f) - j

                l = []
                
                # We have the bounds, iterate THROUGH the contents of the bounds
                ff = f[i:j]
                ii = 0
                tk = ""
                while ii < len(ff):
                    ii+=1
                    cc = ff[ii-1]
                    if cc in WHITESPACE or cc == ff[-1]:
                        if tk:
                            l.append(tk)
                            tk = ""
                        continue
                    elif cc == '"':
                        # find the NEXT quote
                        jj = ff.find('"',ii)
                        if jj == -1: # If we can't, shout
                            raise SyntaxError("Unclosed Spaced Value @ {line}:{character}".format(line=line,character=character))

                        l.append(ff[ii:jj])
                        ii = jj+1
                        continue
                        
                    tk += cc

##                print(l)

                dick[key] = l
                key = ""
                
                # Skip the list for the rest of parsing
                i = j+1
            
            continue

        if c in WHITESPACE or c==f[-1]:
            # We have a character that is seperative a value, so we rely on token
            # However, if there is no token, we assume the current character is the token
            if not token and not c in WHITESPACE:
                token = c
            
            if token and not key:
                key = token
            elif token and not value:
                value = token
            token = ""

##            print(key,value)

            if key and value:
                dick[key] = value
                key = ""
                value = ""

            continue

        token += c

    if key:
        raise EOFError("Key without value @ Line {line}:{character}".format(line=line,character=character))
        
    return dick


if __name__ == "__main__":
    g = parse(f)
    print(g)
